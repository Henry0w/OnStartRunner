package nyc.welles.onstartrunner;

import org.bukkit.Bukkit;
import org.bukkit.command.CommandException;
import org.bukkit.plugin.java.JavaPlugin;

import java.util.ArrayList;
import java.util.List;

public class OnStartRunner extends JavaPlugin {
    @Override
    public void onEnable(){
        this.getConfig().options().header("Add a list of commands you want to run at startup under \"Commands:\", do not use \"/\"");
        saveConfig();
        List<String> defaultCommands = new ArrayList<String>();
        defaultCommands.add("#Your Command Here");
        this.getConfig().addDefault("Commands", defaultCommands);
        getConfig().options().copyDefaults(true);
        saveConfig();
        final List<String> givenCommands = this.getConfig().getStringList("Commands");
        getServer().getScheduler().runTaskLater(this, new Runnable() {
            @Override
            public void run() {
                for (final String i : givenCommands){
                //System.out.println("[RunOnStart] For loop started");
                if (i.startsWith("/")){
                    System.out.println("[RunOnStart] The command " + i + " starts with a slash, and will not be executed");
                }else if(!i.startsWith("#")){
                    System.out.println("[RunOnStart] Executing command: " + i);
                    try{
                        Bukkit.getServer().dispatchCommand(Bukkit.getServer().getConsoleSender(), i);
                        //Bukkit.dispatchCommand(Bukkit.getConsoleSender(), " ");
                    }catch (CommandException e){
                        System.out.println("[RunOnStart] Invalid Command" + i);
                        e.printStackTrace();
                    }
                }
            }
            }
        }, 1); // 1 = next tick

    }
}
